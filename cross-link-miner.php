<?php
/*
Plugin Name:  Cross-link Miner
Plugin URI:   https://gitlab.com/web-tech/cross-link-miner
Description:  By using this simple plugin page editors easily can get suggestion for relevant cross-links. After istallation all previous page and post will go through an analysis process, that provide the further functionality. By selecting words and phrases the cross link miner will search for relevant posts, and will create the crosslinks on both sides.
Version:      201803
Author:       Adam Wagner
Author URI:   https://wagner-bouquet.com
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  clm
Domain Path:  /languages
*/

/*	<----ACTIVATION--->>>
	When CLM plugin is activated we need two new table:
		words and
		post-word links.
*/

function clm_setactive(){
	global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();

	$tables = array();

	//creates `words` table that contian every single word on the site
	$tables[] = "CREATE TABLE `{$wpdb->prefix}clm_words` (
		`word_id` int(10) unsigned not null auto_increment,
		`word` varchar(50),
		`blacklisted` boolean,
		unique index `word_index` (`word`),
		primary key (`word_id`)
	) $charset_collate;";

	//create table that connect posts and words table
	//weight = summarised weight per post
	$tables[] = "CREATE TABLE `{$wpdb->prefix}clm_post_word_links` (
		`word_id` int(10) unsigned not null auto_increment,
		`post_id` bigint(20) unsigned not null,
		`weight` float,
		unique index `ids` (`word_id`, `post_id`),
		index `weight_index` (`weight` desc)
	) $charset_collate;";

	//Let the magic happen.
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $tables );

};
register_activation_hook( __FILE__, 'clm_setactive' );
// during the activation we need to scan the whole site and fill the database, use activation hook for this too. So let`s fill the clm_setactive function with these lines. :)

/** POST processing
*
* When the user publish a post/page
* CLM process the words that
* can be found in the title, lead and content.
*
**/
function clm_process_post( $post_ID, $post_after, $post_before ){
	//Checking if it is an autosave, or it has ben started by the user.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
		}
	//Set globals
	global $wpdb;
	global $post;
	$charset_collate = $wpdb->get_charset_collate();
	//Save CLM words table name into variables
	$clm_words_tablename = $wpdb->prefix . 'clm_words';
	$clm_post_word_links_tablename = $wpdb->prefix . 'clm_post_word_links';
	//Get post parts separately
	$post_title = mb_strtolower( $post->post_title );//in unicode lowercase string
	if ($post->post_excerpt == ''){
		$extended = get_extended( $post->post_content );//used for excerpt and remaining content
		$post_excerpt = mb_strtolower( $extended["main"] );
		$post_content = mb_strtolower( $extended["extended"] );
	}else{
		$post_excerpt = mb_strtolower( $post->post_excerpt );
		$post_content = mb_strtolower ( $post->post_content );
	};
	$full_post = $post_title . ' ' . $post_excerpt . ' ' . $post_content;
	preg_match_all('/(\p{L}|-)+/u', $full_post, $all_words, PREG_PATTERN_ORDER);//Creates variable $words, we need [0] as the first part, full matcing strings
	//prepare parts to weight count IMPORTANT in a different way we can join this 3 array below and use instead of two lines before
	preg_match_all('/(\p{L}|-)+/u', $post_title, $title_words, PREG_PATTERN_ORDER);
	preg_match_all('/(\p{L}|-)+/u', $post_excerpt, $excerpt_words, PREG_PATTERN_ORDER);
	preg_match_all('/(\p{L}|-)+/u', $post_content, $content_words, PREG_PATTERN_ORDER);
	$title_weight_count = array_count_values($title_words[0]);
	$excerpt_weight_count = array_count_values($excerpt_words[0]);
	$content_weight_count = array_count_values($content_words[0]);

	//process each word in foreach cycle below
	foreach ( array_unique( $all_words[0] ) as $word ){
		//insert unique words from content into the database
		$word_data = array( 'word' => $word );
		$wpdb->insert( $clm_words_tablename, $word_data );

		//calculate word weight
		$weight_value = $title_weight_count[$word] * 10 + $excerpt_weight_count[$word] * 3 + $content_weight_count[$word] * 0.1;

		//Add post_ID and word_id and weight to clm_post_word_links
		$word_id = $wpdb->get_var(
			$wpdb->prepare(
				"SELECT `word_id`
				FROM `{$clm_words_tablename}`
				where `word`= %s;",
				$word )); //search for te single word in clm_word table and gives back the id
		$post_word_link_data = array( 'word_id' => $word_id, 'post_id' => $post_ID, 'weight' => $weight_value);
		$wpdb->insert($clm_post_word_links_tablename, $post_word_link_data);

	}

	$wpdb->show_errors();
};
add_action( 'post_updated', 'clm_process_post', 10, 3); //simple save_post action needs to cycle to give back the post content. Using pust_updated action we can get it easier.

/*	<---DISPLAY BOX--->
	On edit post area we need a box where the plugin can communicate.
	In this field every single cross link suggestion will appear after the user select one or more words
 */
function clm_add_displaybox(){
	add_meta_box(
		'clm_display_box',
		'Cross Link Miner suggestions',
		'clm_display_box_html',
		'post'
	);
};
add_action( 'add_meta_boxes', 'clm_add_displaybox' );

function clm_display_box_html($post){

	?>
	<label for='clm_display'>In this field you will get suggestions for potential cross-links</label>
	<div class='postbox'>
		<?php $content; ?> 		sdad
	</div>
	<?php
};
